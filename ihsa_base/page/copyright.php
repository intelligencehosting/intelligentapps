<div class="lime-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="footer-text float-left" id="kep">©<?php echo $year; ?> <a href="https://intelligencehosting.com">Intelligence Hosting</a></span>
            </div>
        </div>
    </div>
</div>
